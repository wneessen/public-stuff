#!/usr/bin/env perl

use strict;
use warnings;
use List::Util qw( shuffle );
use Data::Dumper;

my @chars = shuffle ( 'Mario', 'Luigi', 'Peach', 'Daisy', 'Yoshi', 'Birdo', 'Diddy Kong', 'Bowser Jr.', 'Baby Mario', 'Baby Luigi', 'Baby Peach', 'Baby Daisy', 'Toad', 'Toadette', 'Koopa Troopa', 'Dry Bones', 'Wario', 'Waluigi', 'Donkey Kong', 'Bowser', 'King Boo', 'Rosalina', 'Funky Kong', 'Dry Bowser' );
my @tracks = shuffle ( 'Luigi Circuit', 'Moo Moo Meadows', 'Mushroom Gorge', 'Toad\'s Factory', 'Mario Circuit', 'Coconut Mall', 'DK Summit', 'Wario\'s Gold Mine', 'Daisy Circuit', 'Koopa Cape', 'Maple Treeway', 'Grumble Volcano', 'Dry Dry Ruins', 'Moonview Highway', 'Bowser\'s Castle', 'Rainbow Road', 'GCN Peach Beach', 'DS Yoshi Falls', 'SNES Ghost Valley 2', 'N64 Mario Raceway', 'N64 Sherbet Land', 'GBA Shy Guy Beach', 'DS Delfino Square', 'GCN Waluigi Stadium', 'DS Desert Hills', 'GBA Bowser Castle 3', 'N64 DK\'s Jungle Parkway', 'GCN Mario Circuit', 'SNES Mario Circuit 3', 'DS Peach Gardens', 'GCN DK Mountain', 'N64 Bowser\'s Castle' );
my %points = ( 1 => 15, 2 => 12, 3 => 10, 4 => 8, 5 => 7, 6 => 6, 7 => 5, 8 => 4, 9 => 3, 10 => 2, 11 => 1, 12 => 0 );
my ( $isOk, $rounds, $players, @players, %totals );

print "Mario Kart Wii v2.0\n";
print "===================\n";

while( !$isOk ) {
    print "Number of rounds (3-32): ";
    $rounds = <STDIN>;
    chomp($rounds);
    if( $rounds eq '' or $rounds =~ m/[A-Za-z]+/ ) {
        print "Not a number. Try again.\n";
        next;
    }
    elsif( $rounds <= 2 or $rounds > 32 ) {
        print "Invalid amount of rounds (3 to 32). Try again.\n";
        next;
    }
    else {
        $isOk = 1;
    }
}
undef $isOk;

while( !$isOk ) {
    print "Number of players (1-4): ";
    $players = <STDIN>;
    chomp($players);
    if( $players eq '' or $players =~ m/[A-Za-z]+/ ) {
        print "Not a number. Try again.\n";
        next;
    }
    elsif( $players <= 0 or $players > 4 ) {
        print "Invalid amount of players (1 to 4). Try again.\n";
        next;
    }
    else {
        $isOk = 1;
    }
}

my $i = 1;
while( $i <= $players ) {
    print "Name of player $i: ";
    my $name = <STDIN>;
    chomp($name);
    if( grep( /^$name$/, @players ) ) {
        print "Name already in use. Choose a different one.\n";
        next;
    }
    push( @players, $name );
    $i++;
}
for( my $i = 1; $i <= (12-$players); $i++ ) {
    push( @players, shift @chars );
}

for( my $j = 1; $j <= $rounds; $j++ ) {
    print $j . '. Round (' , shift @tracks , ')' . "\n";
    print '=' x 30 . "\n";
    my @winners = shuffle @players;
    for( my $x = 0; $x <= $#players; $x++ ) {
        my $y = $x + 1;
        print sprintf( '%02d', $y ) . '. Position => ' . $winners[$x] . ' (' . $points{$y} . ' points)' . "\n";
        $totals{ $winners[$x] } += $points{$y};
    }
    print "\n";
}

print "Totals\n";
print '=' x 30 . "\n";
my $count = 1;
foreach my $total ( reverse sort { $totals{$a} <=> $totals{$b} } keys %totals ) {
    print sprintf( '%02d', $count ) . '. Position => ' . $total . ' (' . sprintf( '%02d', $totals{ $total } ) . ' points)' . "\n";
    $count++;
}
